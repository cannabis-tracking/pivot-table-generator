#!/usr/bin/env python3

"""Create a pivot table from a Metrc CSV report."""

import datetime
from pathlib import Path

import pandas as pd


def main() -> int:
    # Setup
    today = datetime.date.today()
    date = f"{today.month}.{today.day}.{today.year}"
    report_file = Path("PlantsDestroyedReport.csv").resolve()
    report_table_file = Path(f"{date} {report_file.stem} ReportPivotTable.csv").resolve()
    strain_table_file = Path(f"{date} {report_file.stem} StrainPivotTable.csv").resolve()
    # Open destroy report
    destroy_report = pd.read_csv(report_file)
    
    # Create kills report pivot table
    pivot_index = ["Location", "Destroyed Note"]
    pivot_table = pd.pivot_table(destroy_report,
                                 index=pivot_index,
                                 aggfunc=[len],
                                 values=["Plant"],
                                 margins=True,
                                 dropna=True,
                                 fill_value=0)

    # Get Veg strains only
    veg_destroy_report = destroy_report[destroy_report["Location"].str.contains("Veg")]
    # Filter out Moms
    veg_filtered_report = veg_destroy_report[~veg_destroy_report["Location"].str.contains("MOMS")]

    # Create Veg strains pivot table
    pivot_index = ["Strain"]
    strains_pivot_table = pd.pivot_table(veg_filtered_report,
                                         index=pivot_index,
                                         aggfunc=[len],
                                         values=["Plant"],
                                         margins=True,
                                         dropna=True,
                                         fill_value=0)
    
    pivot_table.to_csv(report_table_file)
    strains_pivot_table.to_csv(strain_table_file)
    
    return 0


if __name__ == "__main__":
    main()
