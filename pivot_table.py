#! /usr/bin/env python3

"""Creates a spreadsheet containing a pivot table that is formatted by
a JSON file."""

from pathlib import Path

import xlrd
# import numpy as np
import pandas as pd


def main():
    Inventory_Filename = Path("PlantsDestroyedReport.xls").resolve()
    Header = 12
    # USECOLS = [2, 5, 6, 7]
    TagCol = "Plant"
    NoteCol = "Destroy Note"
    GroupCol = "Plant Group"
    # StrainCol = "Strain"
    ROOM_COL = "Location"
    USECOLS = [2, 5, 6, 8]
    Daily_ReportIndex = [ROOM_COL, NoteCol]
    # Weekend_Index = [StrainCol] + Weekday_Index

    # df.iloc[0::5, :]  # every fifth row
    # Destroy Note on Odd Lines Past 13 (15-n)

    # Get Date
    wb = xlrd.open_workbook(Inventory_Filename)  # open inventory workbook
    ws = wb.sheet_by_index(0)  # get the first/only worksheet
    date_cell = ws.cell_value(2, 4)  # get date range from inventory report
    date_cell = date_cell.split()  # get list split by whitespace
    date = date_cell[3].replace('/', '.')
    # 3rd word in cell is the most recent date

    # Formatted strings
    PivotTable_Filename = f"{date} {Inventory_Filename.stem} PivotTable.xlsx"

    # Build table using pandas
    destroyed_report = pd.read_excel(Inventory_Filename,
                                     header=Header,
                                     usecols=USECOLS)
    filtered_report = destroyed_report.iloc[0::2, :]  # get every other row
    del filtered_report[GroupCol]  # delete "Plants Group" column w/o note
    destroy_notes = destroyed_report.iloc[1::2, :]  # above, starting at 1
    destroy_notes = destroy_notes[GroupCol]  # get just notes column

    filtered_report = filtered_report.reset_index(drop=True)
    # index using new table, not orignal
    destroy_notes = destroy_notes.reset_index(drop=True)  # reindex

    final_table = pd.concat([filtered_report[TagCol], destroy_notes,
                             filtered_report.iloc[:, 1::]],
                            axis=1)

    # Replace plant group with destroy notes
    del final_table[GroupCol]
    final_table.insert(1, NoteCol, destroy_notes[:],
                       allow_duplicates=False)
    # final_table[GroupCol] = destroy_notes[:]
    # final_table.rename(columns={GroupCol: NoteCol}, inplace=True)

    # Create pivot table
    # index = rows
    # columns = columns
    # values = what to look at
    # aggfunc = what to do to values
    # # consider changing daily report order # #
    pivot_table = pd.pivot_table(final_table,
                                 index=Daily_ReportIndex,
                                 # columns=["Product"],
                                 aggfunc=[len],
                                 # can use dictionary: aggfunc={
                                 # ":Quanity": len, "Price": [np.sum, np.mean]}
                                 # np = numpy
                                 values=[TagCol],
                                 margins=True,
                                 dropna=True,
                                 fill_value=0)
    pivot_table.to_excel(PivotTable_Filename)

    print(f"Saved to {PivotTable_Filename}")


if __name__ == "__main__":
    main()


""" CODE DUMP
If your string constraint is not just one string you can drop those
corresponding rows with:

df = df[~df['your column'].isin(['list of strings'])]

The above will drop all rows containing elements of your list

Keeping rows instead of dropping:
Just remove the "~" df = df[df['your column'].isin(['list of strings'])]


Let’s see how to get all rows in a Pandas DataFrame containing given substring
with the help of different examples.

Code #1: Check the values PG in column Position
filter_none

edit

play_arrow

brightness_4
# importing pandas
import pandas as pd

# Creating the dataframe with dict of lists
df = pd.DataFrame({'Name': ['Geeks', 'Peter', 'James', 'Jack', 'Lisa'],
                   'Team': ['Boston', 'Boston', 'Boston', 'Chele', 'Barse'],
                   'Position': ['PG', 'PG', 'UG', 'PG', 'UG'],
                   'Number': [3, 4, 7, 11, 5],
                   'Age': [33, 25, 34, 35, 28],
                   'Height': ['6-2', '6-4', '5-9', '6-1', '5-8'],
                   'Weight': [89, 79, 113, 78, 84],
                   'College': ['MIT', 'MIT', 'MIT', 'Stanford', 'Stanford'],
                   'Salary': [99999, 99994, 89999, 78889, 87779]},
                   index =['ind1', 'ind2', 'ind3', 'ind4', 'ind5'])
print(df, "\n")

print("Check PG values in Position column:\n")
df1 = df['Position'].str.contains("PG")
print(df1)

Output:


But this result doesn’t seem very helpful, as it returns the
bool values with the index. Let’s see if we can do something better.

Code #2: Getting the rows satisfying condition
filter_none

edit

play_arrow

brightness_4
# importing pandas as pd
import pandas as pd

# Creating the dataframe with dict of lists
df = pd.DataFrame({'Name': ['Geeks', 'Peter', 'James', 'Jack', 'Lisa'],
                   'Team': ['Boston', 'Boston', 'Boston', 'Chele', 'Barse'],
                   'Position': ['PG', 'PG', 'UG', 'PG', 'UG'],
                   'Number': [3, 4, 7, 11, 5],
                   'Age': [33, 25, 34, 35, 28],
                   'Height': ['6-2', '6-4', '5-9', '6-1', '5-8'],
                   'Weight': [89, 79, 113, 78, 84],
                   'College': ['MIT', 'MIT', 'MIT', 'Stanford', 'Stanford'],
                   'Salary': [99999, 99994, 89999, 78889, 87779]},
                   index =['ind1', 'ind2', 'ind3', 'ind4', 'ind5'])

df1 = df[df['Position'].str.contains("PG")]
print(df1)

Output:


Code #3: Filter all rows where either Team contains ‘Boston’ or
College contains ‘MIT’.
filter_none

edit

play_arrow

brightness_4
# importing pandas
import pandas as pd

# Creating the dataframe with dict of lists
df = pd.DataFrame({'Name': ['Geeks', 'Peter', 'James', 'Jack', 'Lisa'],
                   'Team': ['Boston', 'Boston', 'Boston', 'Chele', 'Barse'],
                   'Position': ['PG', 'PG', 'UG', 'PG', 'UG'],
                   'Number': [3, 4, 7, 11, 5],
                   'Age': [33, 25, 34, 35, 28],
                   'Height': ['6-2', '6-4', '5-9', '6-1', '5-8'],
                   'Weight': [89, 79, 113, 78, 84],
                   'College': ['MIT', 'MIT', 'MIT', 'Stanford', 'Stanford'],
                   'Salary': [99999, 99994, 89999, 78889, 87779]},
                   index =['ind1', 'ind2', 'ind3', 'ind4', 'ind5'])


df1 = df[df['Team'].str.contains("Boston") | df['College'].str.contains('MIT')]
print(df1)

Output:


Code #4: Filter rows checking Team name contains ‘Boston and Position
must be PG.
filter_none

brightness_4
# importing pandas module
import pandas as pd

# making data frame
df = pd.read_csv("https://media.geeksforgeeks.org/wp-content/uploads/nba.csv")


df1 = df[df['Team'].str.contains('Boston') & df['Position'].str.contains('PG')]
df1

Output:



Code #5: Filter rows checking Position contains PG and College must
contains like UC.
filter_none

brightness_4
# importing pandas module
import pandas as pd

# making data frame
df = pd.read_csv("https://media.geeksforgeeks.org/wp-content/uploads/nba.csv")


df1 = df[df['Position'].str.contains("PG") & df['College'].str.contains('UC')]
df1

Output:

DataFrame.insert(self, loc, column, value, allow_duplicates=False)[source]¶

Insert column into DataFrame at specified location.

Raises a ValueError if column is already contained in the DataFrame,
unless allow_duplicates is set to True.
Parameters:

loc : int

Insertion index. Must verify 0 <= loc <= len(columns)
column : string, number, or hashable object

label of the inserted column
value : int, Series, or array-like
allow_duplicates : bool, optional


DataFrame.loc¶

Access a group of rows and columns by label(s) or a boolean array.

.loc[] is primarily label based, but may also be used with a
boolean array.
"""
"""
DataFrame.filter(self, items=None, like=None, regex=None,
axis=None)[source]¶

Subset rows or columns of dataframe according to labels in the
specified index.

Note that this routine does not filter a dataframe on its
contents. The filter is applied to the labels of the index.
Parameters:

items : list-like

Keep labels from axis which are in items.
like : string

Keep labels from axis for which
“like in label == True”.
regex : string (regular
expression)

Keep labels from axis for which
re.search(regex, label) == True.
axis : int or string axis name

The axis to filter on. By default
this is the info axis, ‘index’ for
Series, ‘columns’ for DataFrame.
Returns:

same type as input object
"""
